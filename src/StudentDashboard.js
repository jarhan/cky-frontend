import React from 'react';
// import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
// import FlatButton from 'material-ui/FlatButton';
// import Paper from 'material-ui/Paper';
// import Divider from 'material-ui/Divider';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import CardView from './CardView';
import * as api from "./api";

const catagory = {
    // position: absolute,
    // top: -34,
    // left: 0,
    width: 90,
    // "margin-left": 1110,
    background: "#7aa438",
    padding: "10px 15px",
    color: "#FFFFFF",
    "font-size": 14,
    "font-weight": 600,
    "box-shadow": "0 3px 11px 0 rgba(0, 0, 0, 0.2), 0 3px 11px 0 rgba(0, 0, 0, 0.19)"
    // "text-transform": uppercase,
}

export default class StudentDashboard extends React.Component {

    handleChange = (event, index, value) => this.setState({value});
    onAdd = (props) => {
        api.addStudentToCourse(this.state.myId, props.id).then(this.fetchData)
    }
    onRemove = (props) => {
        api.removeStudentFromCourse(this.state.myId, props.id).then(this.fetchData)
    }
    fetchData = () => {

        api.getMyCourse(this.state.myId).then(response => {
            this.setState({myCourses: response.courses})}).then(() => {
                return   api.getAllCourse().then(ret => {
                        this.setState({allCourses: ret.courses});
                })
        })
    }

    constructor(props) {
        super(props);
        this.state = {
            value: 1,
            myId: 0,
            combinedTable: [],
            myCourses: [],
            allCourses: []
        };

    }

    componentDidMount() {
        api.whoami().then(ret => {
            this.setState({myId: ret.id})
        })
        this.fetchData()
    }

    render() {

        const {allCourses, myCourses} = this.state

        return (
            <div>
                <section>
                    <h1>Courses</h1>
                    <br/>
                    {myCourses.map((each) => {
                        return <CardView courseName={each.name} instructorName={each.lecturer} time={each.classTime}
                                         students={each.students}
                                         added={true} id={each.id} onRemove={this.onRemove.bind(this)}/>
                    })}
                    {allCourses.filter(course => !myCourses.map(each => each.name).includes(course.name)).map((each) => {
                        return <CardView courseName={each.name} instructorName={each.lecturer} time={each.classTime}
                                         students={each.students}
                                         id={each.id} onAdd={this.onAdd.bind(this)}/>
                    })}
                </section>
            </div>
        );
    }
}
